;; tutorial-condicionales.el

;; Copyright (C) 2021, Maxxcan
;; This file is part of a Maxxcan Emacs Lisp tutorial, released under GNU GPL with ABSOLUTE NO WARRANTY. See the file COPYING for details.

;; Este tutorial está basado en el tutorial creado por Thien-thi Nguyen que podemos encontrar <<aquí>>

;; Los bucles nos permiten ejecutar un bloque de código un número determinado de veces. Éste además se repetirá mientras ocurra alguna condición o hasta que ésta deje de ocurrir.

;; Vamos a ver los constructores que tenemos en Emacs Lisp para manejar bucles.

;; 1. loop.        Es la forma más fácil de hacer un bucle. Ejecuta una declaración hasta que encuentra un `return'
;; 2. loop for     Nos permite hacer un bucle tal como se hace en los lenguajes más comunes.
;; 3. do           Otra forma de implementar bucles
;; 4. dotimes      Nos permite hacer bucles un número determinado de veces
;; 5. dolist       Nos permite hacer iteraciones con cada elemento de una lista


;; Vamos a ver el constructor Loop

;; Como hemos dicho es la forma más fácil de hacer un bucle y ejecuta una declaración hasta que encuentra `return'

;; >>> Evalúa las siguientes expresiones

(setq a 10)

(loop
 (setq a (+ a 1))
 (print a)
 (when (> a 17)(return a)))



;; Como vemos nos nuestra una sucesión de números que van desde 10 hasta 18, ya que le hemos dicho que cuando a sea mayor que 17 nos devuelva el valor de a.

;; Vamos ahora con el constructor Loop For.

;; Como ya he dicho este constructor nos permite implementar un bucle tal y como se hace en la mayoría de los lenguajes de programación. Esto nos permite:

;; - Actualizar variables por iteración.
;; - Especificar expresiones que condicionalmente terminan la iteración. Es decir, termina cuando pase lo siguiente.
;; - Especificar expresiones para implementar algún trabajo en cada iteración. Es decir, hacer determinada cosa en cada iteración.
;; - Especificar expresiones que hacen algo antes de salir del bucle.

;; Vamos con los ejemplos que así es más sencillo de ver.

;; >>> Evaluemos el siguiente código


(loop for x in '(pedro juan antonio)
      do (print x))

(loop for a from 10 to 20
      do (print a))

(loop for x from 1 to 20
      if(evenp x)
      do(print x))


;; Espero que solo al observar el resultado de los ejemplos se entienda como funciona `loop for' 


;; Vamos ahora con Do.

;; Es otra forma de implementar un bucle. Su sintaxis básica es:

(do ((variable1 valor1 valor-actualizado)
     (variable2 valor2 valor-actualizado)
     ...)
    (test valor-de-retorno)
  (s-expresión))

;; Vamos a ver si explicamos esto. El valor inicial de cada variable es evaluado y enlazado a su respectiva variable. El valor actualizado corresponde a una declaración de actualización opcional que dice cómo el valor será actualizado en cada iteración.

;; Después de cada iteración, el test es evaluado y si devuelve un valor no nulo, el valor devuelto será evaluado y devuelto.

;; La última (s-expresión) es opcional. Si está presente será ejecutada después de cada iteración hasta que se devuelva el valor true o verdadero.

;; Vamos a ver unos ejemplos para dejarlo más claro.

;; >>> Evalúa las siguientes expresiones

(do ((x 0 (+ x 2)) ;; el valor inicial de x es 0 y el valor actualizado es x + 2
     (y 20 (- y 2))) ;; el valor inicial de y es 20 y su valor actualizado es y - 2
    ((= x y))  ;; aquí hacemos la prueba que es si x es igual a y, entonces a x le restamos y
  (print x) (print y)) ;; escribiremos x = (su valor) y = (su valor) hasta x sea igual a y




(dotimes (n 11)
  (print n) (print (* n n))


  ;; Ahora vamos con `Dotimes', el cual nos permite hacer bucles con un número fijo de iteraciones.

  ;; >>> Evalúa el siguiente código.

  (dotimes (n 11)
    (print (* n n)))


  ;; Y terminamos con `Dolist'

  ;; Este constructor nos permite hacer iteraciones con cada uno de los elementos de la lista.

  ;; >>> Evalúa la siguiente expresión y observa el resultado
  

  (dolist (n '(1 2 3 4 5 6 7 8 9))
    (print (* n n))
    )


;; A modo de resumen dejamos claro que las formas especiales que tenemos para crear bucles son principalmente:

;; 1. loop.        Es la forma más fácil de hacer un bucle. Ejecuta una declaración hasta que encuentra un `return'
;; 2. loop for     Nos permite hacer un bucle tal como se hace en los lenguajes más comunes.
;; 3. do           Otra forma de implementar bucles
;; 4. dotimes      Nos permite hacer bucles un número determinado de veces
;; 5. dolist       Nos permite hacer iteraciones con cada elemento de una lista







;;; Local variables:
;;; mode: lisp-interaction
;;; End:

;;; el tutorial sobre las variables termina aquí



