;; tutorial-variables.el

;; Copyright (C) 2021, Maxxcan
;; This file is part of a Maxxcan Emacs Lisp tutorial, released under GNU GPL with ABSOLUTE NO WARRANTY. See the file COPYING for details.

;; Este tutorial está basado en el tutorial creado por Thien-thi Nguyen que podemos encontrar <<aquí>>

;; Las variables son espacios de memoria donde se almacenan valores. Se llaman variables porque en ese espacio puede cambiar el valor que se guarda en él. Para poder recuperar los valores en esos espacios de memoria le indicamos un nombre con el que nos referimos a ese espacio. Veamos esto con una analogía.

;; Imaginemos que queremos aparcar nuestro coche en un garage de un centro comercial. La plaza de garage es el espacio de memoria donde va a ir nuestro coche, que es el valor de ese espacio de memoria. Por otro lado, los garages de los centros comerciales suelen tener un nombre, que dependerá de cada centro comercial, pero podría ser por ejemplo azul-120 o rojo-20 y ese nombre nos ayuda a poder recuperar el valor de la plaza de garage, es decir, nuestro coche.

;; Todos los lenguajes que se precien tienen una forma de asignar un valor a ese espacio de memoria y darle un nombre para poder recuperar ese valor.

;; En Emacs Lisp nos vamos a encontrar con cuatro formas principales:

;; 1. defvar
;; 2. defconst
;; 3. setq
;; 4. let
;; 5. let* 

;; Las dos primeras son una forma de crear variables globales, mientras que las dos últimas son para crear variables locales. Para saber que es una cosa y la otra tendremos que hablar de los ámbitos de la variables.

;; El ámbito de las variables o solamente ámbito (ya que viene del inglés scope) es el que determina en qué partes del programa una entidad puede ser usada. En nuestro caso determina en qué parte del programa podemos acceder al valor de una variable.

;; Si una variable es declarada dentro de un bloque (método/función/procedimiento), ésta será válida solo dentro de ese bloque y se destruirá al terminar el bloque. A esa variable la vamos a llamar variable local. Veamoslo con un ejemplo

;; >> Evaluemos las siguientes las siguientes expresiones



;; variable global
(defvar a 8)

(print a)

;; variable local
(let ((a 1)) (print a))

(print a)


;; Vemos que si pulsamos `C-j' después de la expresión (print a) después de haberlo hecho en (defvar a 8) nos devolverá el número 8 pero si lo hacemos en la expresión con let nos devolverá otro número que es 1. Si volvemos a evaluar la última expresión nos volverá a devolver el número 8. Como vemos primero hemos declarado una variable global y con let una local, que solo se puede acceder dentro de su bloque.

;; Diferencia entre defvar y setq.

;; Las variables creadas con `defvar' son llamadas constantes porque no pueden volver a cambiarse otra vez dándoles otro valor con `defvar'. Estas variables solo se le puede cambiar su valor con setq el cual a su vez una variable establecida con setq solo puede ser cambiado su valor con `setq' de nuevo.

;; >> Evaluemos las siguientes expresiones


(defvar a 3)

(print a)

(defvar a 4)

(print a)

(setq a 5)

(print a)

(setq a 6)

(print a)


;; Además de defvar también tenemos que defconst. Es muy similar a defvar pero con la diferencia de que irónicamente `defconst' sí nos permite volver a definir una variable que ya hayamos definido con `defconst'. Veamos el siguiente ejemplo:

;; >>> Evalúa las siguientes empresiones


(defconst b 2)

(print b)

(defconst b 3)

(print b)

(defvar b 4)

(print b)

(defconst b 5)

(print b)


;; Además de la diferencia entre `setq' y `defvar' (no le des mucha importancia de momento a defconst) lo bueno que tiene `defvar' y `defconst' respecto a `setq' es que podemos añadir un texto explicativo sobre qué es la variable, un llamado docstring. Este texto es muy útil para saber qué es la variable.

;; >>> Evalúa la siguiente expresión

(defvar pi 3.14159 "Una aproximación de pi")


(defconst e 2.71828 "Una aproximación del número e")


(defvar mi-peso 110 "Mi peso sin comer")



;; Si ponemos el cursor sobre pi y e, respectivamente en el mini-buffer veremos que aparece la explicación o docstring que hemos escrito.

;; También podemos pulsar `C-h-v' y buscar esas variables que ya estarán accesibles y veréis lo que dice.


;; Ya que hemos visto las variables globales, vamos a ver las variables locales que ya hemos visto que se declaran principalmente con `let'. Pero también tenemos `let*', así que veamos la diferencia entre ambas.

;; let vs let*


;; Como ya hemos visto let nos permite crear variables locales, pero con let vamos a tener un pequeño inconveniente y es que no podemos hacer que una variable acceda a otra. Veámoslo con un ejemplo.

;; >>> Evalúa la siguiente expresión

(setq y 2)

(let ((y 1)
      (z y))
  (list y z))


;; Como vemos z tiene el valor de 1 tal como le hemos dicho, pero z en vez de tener el valor de y dentro de let tiene el que le hemos indicado con setq.

;; Esto ocurre porque digamos que la asigación de un valor a su variable no ocurre de forma secuencia tal como lo podríamos imaginar sino de una forma en paralelo. Así que no puede haber una relación entre una variable y otra dentro de let.

;; Además, podemos darle el valor a una variable dos veces con el mismo let.

(let ((x 1)
      (x 2))
  (print x))

;; Ahora vamos a usar let*, el cual lo que hace es que hace que se pueda acceder a la variable local dentro de let antes de irse a la variable local con setq. Comparemos resultados haciendo lo mismo del ejemplo anterior pero usando let*.

;; >>> Evalúa las siguientes expresiones

(setq y 2)

(let* ((y 1)
       (z y))
  (list y z))


;; Como vemos ahora y sí vale 1 y no 2. Esto sería porque con let* hay una serialización de la asignación de las variables. Realmente let* es como si hiciesemos let de forma recursiva. Veámoslo

;; >>> Evaluemos lo siguiente

(setq y 2)


(let (( y 1))
  (let (( z y))
    (list y z)))

;; Vemos que obtenemos el mismo valor que con let*. Así que digamos que let* es una forma más cómoda de hacer un let recursivo. Y nos ahorramos paréntesis además que ya se nos caen.

;; Algunas personas se preguntan por qué la forma de let en paralelo es lo normal y no hacerlo de forma secuencia de forma predeterminada. Aquí os tengo que recordar que aunque no lo he dicho let y let*, al igual que algunas otras cosas que hemos usado no son primitivas del lenguaje, esto es, funciones propias del lenguaje, sino que se les llama formas especiales porque son creadas a partir de esas formas primitivas, en concreto let y let* se han construido a partir de una primitiva llamada lambda.

;; El tema y resumiendo, es que en general es mejor usar let y no tener variables que dependan de otras variables ya que esto además de confuso a la hora de leer el código puede dar lugar a errores y efectos que nos pueden costar identificar. Así que en la mayoría de los casos usar let y solo en casos muy puntuales y si es estrictamente necesario usar let*.


;; -------------------------------------------------------------------------------------------------------------

;; >>>>>>>>Esto no lo hace falta que lo leas. Es más una curiosidad.<<<<<<<<<<<<<

;; Como he dicho let es una forma especial, no una primitiva real. La primitiva real es lambda. Así let puede ser sustituido por esta:

;; >>> Evaluemos las siguientes expresiones.


(let ((a 1) (b 2))(list a b))

((lambda (a b) (list a b)) 1 2)

(let ((a 1) (b a))(list a b))

El último let evidentemente da error.

De forma similar let* puede ser sustituido también por lambda.

;; >>> Evaluemos las siguientes expresiones

(let* ((a 1) (b a))(list a b))


((lambda (a)
   ((lambda (b)
      (list a b))
    a))
 1)






;;; Local variables:
;;; mode: lisp-interaction
;;; End:

;;; el tutorial sobre las variables termina aquí




