;; tutorial-condicionales.el

;; Copyright (C) 2021, Maxxcan
;; This file is part of a Maxxcan Emacs Lisp tutorial, released under GNU GPL with ABSOLUTE NO WARRANTY. See the file COPYING for details.

;; Este tutorial está basado en el tutorial creado por Thien-thi Nguyen que podemos encontrar <<aquí>>

;; Lo interesante de la programación no es solo que le administremos datos y que las funciones hagan cosas con ellos, tipo calculadora. Lo interesante es que además las funciones hagan una cosa u otra dependiendo de ciertas cosas. Esto le da flexibilidad al programa que le permite hacer distintas cosas en distintos escenarios.

;; Para ello vamos a ver ciertas primitivas que nos ayudarán a ello.

;; Declaración If

;; La expresión `IF' es la más simple y común de los condicionales y es habitual en la mayoría de los lenguajes de programación. En Emacs Lisp es una función especial.

;; La expresión `IF' toma tres argumentos: una prueba, una parte verdadera y una parte falsa. Si la prueba es verdadera, `IF' devolverá el valor de la parte verdadera, si el test es falso, escapará de la parte verdadera y nos devolverá el valor de la parte falsa. Veámoslo con algunos ejemplos:

;; >>> Evalúa las siguientes expresiones


(if (oddp 1) 'impar 'par)

(if (oddp 2) 'impar 'par)

(if (symbolp 'hola) (* 5 5) (+ 5 5))

(if (symbolp 1) (* 5 5) (+ 5 5))



;; En la mayoría de lenguajes se usa por un lado la expresión o función `IF' y por otro `ELSE', pero en Emacs Lisp todo lo hace `IF'. Pongamos unos comentarios para aclarar esto:

;; >>> Evalúa las siguientes expresiones

(setq x 2)

(if (> x 0)
    (message "El número es positivo")
  ;; En otros lenguajes aquí iría un ELSE, en plan Si X es mayor que 0 escribe "El número es positivo", pero SINO
  (message "El número es negativo"))

;; >>> Juega con los valores de x e incluso con las condiciones para ver los resultados

;; La forma especial `WHEN' Y `UNLESS'.

;; Veamos estos dos ejemplos:

;; >>> Evalúa las siguientes expresiones y observa los resultados

(when t 3)

(unless t 3)

(when (> 5 3)
  (message "cinco es mayor que 3"))

(unless (> 5 3)
  (message "5 es mayor que 3"))

(unless (> 2 3)
  (message "2 es menor que 3"))


;; Como vemos `when' es una forma rápida y de decir, cuando ocurra tal cosa, ejecuta algo, y `unless' es su contrario.


;; Vamos a ver ahora la forma especial `COND' 

;; La forma especial `cond' es el condicional más clásico en Lisp. De hecho es el primero que se creó y de él vienen los demás. La entrada consiste en un número de pruebas y consecuencias. La forma general es: (cond (prueba-1 consecuencia-1)(prueba-2 consecuencia-2)... (prueba-n consencuencia-n))

;; Veamoslo con un ejemplo rápido:

;; >>> Evalúa las siguientes expresiones y observa los resultados

(defun comparar (x y)
  (cond ((equal x y) 'numeros-iguales)
	((< x y) 'el-primero-es-menor)
	((> x y) 'el-primero-es-mayor)))

(comparar 3 5)

(comparar 7 2)

(comparar 4 4)

;; Y si no se cumple ninguna de las opciones propuestas? Pues para eso tenemos la palabra mágica true o t. Así de esta forma:

;; >>> Evalúa la siguiente expresíon:

(defun capital-de (x)
  (cond ((equal x 'Francia) 'Paris)
	((equal x 'España) 'Madrid)
	(t 'esa-no-me-la-se)))


(capital-de 'España)

(capital-de 'Francia)

(capital-de 'Italia)


;; Veamos ahora las macro `AND' y `OR'

;; Las macros `AND' y `OR' nos permiten hacer expresiones más complejas. Veamos el siguiente ejemplo.

(defun test (x y)
  (or (> x y)
      (zerop x)
      (zerop y)))

(test 2 3)

(test 0 1)

(test 1 0)

;; Como podemos observar este test nos nuestra si alguno de los elementos es un 0.

;; Pero `AND' y `OR' tienen diferentes significados en Emacs Lisp distintos a nuestra lógica diaria. Así que las reglas son las siguientes:

;; `and', evalúa las clausulas una a la vez. Si una de ellas devuelve `nil', se detiene y devuelve `nil' \; sino sigue a la siguiente. Si ninguna devuelve nil, entonces `and' devolverá el valor de la última clausula. Veamos unos ejemplos sencillos:

;; >>> Evalúa esas expresiones

(and nil t t)

(and 'jorge nil 'paco)

(and 'jorge 'paco 'juan)

;; `or', en cambio, evalúa las clausulas una a la vez y en cuanto una de ellas devuelve un valor distinto a `nil', para y devuelve ese valor. Veamos unos ejemplos

;; >>> Evalúa las siguientes expresiones y observa los resultados

(or nil t t)

(or 'jorge nil 'juan)

(or 'jorge 'paco 'juan)

(or nil 'juan 'paco)

;; Vamos a hacer unos ejercicios. Os voy a dejar unas expresiones más y evaluarlas después de pensar mentalmente cual va a ser el resultado y así ver si se ha entendido.

;; >>> Evalúa estas expresiones después de hacerlas de cabeza

(and 'juan 'paco 'pedro)

(or 'juan 'paco 'pedro)

(or nil 'paco nil)

(and (equal 'abc 'abc) 'si)

(or (equal 'abc 'abc) 'si)


;; Bueno hasta aquí las formas más importantes de hacer condicionales que resumiendo son:

;; 1. if

;; 2. when y unless

;; 3. and y or

;; 4. cond


;; -------------------------------------------------------------------------------------------------------------

;; >>>>>>>>Esto no lo hace falta que lo leas.<<<<<<<<<<<<<

;; A partir de aquí vamos a ver cosas más complejas relacionadas con los condicionales. No es necesario de momento pero tal vez te pueda ayudar en un futuro.

;; Vamos a ver como podemos hacer funciones más complejas gracias al uso de `and' y `or'.

;; ¿Os acordáis de esta función?

(defun capital-de (x)
  (cond ((equal x 'Francia) 'Paris)
	((equal x 'España) 'Madrid)
	(t 'esa-no-me-la-se)))

;; Pues vamos a añadirle algo más de complejidad para añadir varias formas de introducir el nombre del país.

(defun capital-de (x)
  (cond (and (equal x 'paris) (equal x 'Paris) 'Francia)
	;; (and (equal x 'Inglaterra)(equal x 'inglaterra)(equal x 'Ingland) ('London)
	;; (and (equal x 'España)(equal x 'españa)(equal x 'Spain) ('Madrid)
	(t 'esa-no-me-la-se)))





;;; Local variables:
;;; mode: lisp-interaction
;;; End:

;;; el tutorial sobre los condicionales termina aquí
