;; tutorial-funciones.el

;; Copyright (C) 2021, Maxxcan
;; This file is part of a Maxxcan Emacs Lisp tutorial, released under GNU GPL with ABSOLUTE NO WARRANTY. See the file COPYING for details.

;; Este tutorial está basado en el tutorial creado por Thien-thi Nguyen que podemos encontrar <<aquí>>

;; Bien ahora que ya sabemos sobre las variables vamos a ver que podemos hacer con ellas y es que para programar necesitamos por un lado los datos -variables- y una forma de trabajar con ellas. Para eso tenemos las funciones que cogen datos de entrada y producen datos de salida.

;; Para definir una función tenemos que usar la forma especial `defun' pero antes de eso veamos las funciones anónimas.

;; Una función anónima es una función a la que no asignamos ningún nombre. Veamoslo.

;; >>> Evaluémos lo siguiente:

(lambda (x) (+ x 3))


;; Bien ahora hemos definido esa función pero no hace mucho y para volver a llamarla pues tenemos que escribirla de nuevo. Pero al menos ya sabemos como definir una función. Pero para qué sirve una función anónima?, bueno ya veremos que una de las cosas potentes de este lenguaje es que podemos usar funciones como argumentos de otras funciones pero eso lo veremos en un futuro. Pero vamos a lo de ahora. Vamos a ejecutar nuestra función pero dándole un valor a nuestra variable x.

;; >>> Evaluemos la siguiente expresión

((lambda (x) (+ x 3)) 4)

;; Ahora nuestra función sí hace algo. Coge el 4 que le hemos introducido como valor de entrada y le suma un 3 y da como resultado un 7. Claro el caso sigue siendo que si queremos repetir el proceso tenemos que volver a escribirlo todo. Vamos a darle un nombre a nuestra función para que sea más fácil llamarla.

;; >>> Evaluemos la siguiente expresión

(defvar sumar3 (lambda (x) (+ x 3)))

;; Ahora podemos usarla gracias a la primitiva `funcall' que como su nombre indica llama a las funciones.

;; >>> Evaluemos la siguiente expresión

(funcall sumar3 10)

;; Bien ahora hemos sumado 3 a 10. Muy bien verdad?, pero un poco follonero ya que hemos tenido que escribir la función, darle un nombre con defvar y luego llamarla con funcall. Bueno para solucionar todo eso a la vez y no tener que escribir tanto tenemos a `defun'. Así que todo lo anterior lo podemos resumir en...

;; >>> Evaluemos la siguiente expresión

(defun sumar4 (x)
  (+ x 4))


(sumar4 10)

;; Y ya está. Vale que es mucho más fácil y me podría haber ahorrado todo lo anterior pero así os he explicado lo que son las funciones anónimas y todo lo que realmente hace `defun'.

;; Ahora vamos a ver las funciones y sus argumentos.

;; Como hemos visto en el ejemplo  anterior la función sumar3 o sumar 4 tenía un argumento, esto es, un dato que tenemos que introducirlo para ejecutar la función y que si no lo hacemos nos daría un error. Pruébalo si eso. Y ese argumento va después del nombre de la función entre paréntesis. Podemos hacer funciones sin argumentos?, claro que sí:

;; >>> Evalúa este ejemplo

(defun saludos ()
  (print "Hola a todos!"))




;; Ahora vamos a ver una función con valores obligatorios y otros opcionales.

;; >>> Evalúa las siguientes expresiones

(defun mostrar-elementos (a b &optional c d)
  (list a b c d))


(mostrar-elementos 1)

(mostrar-elementos 1 2)

(mostrar-elementos 1 2 3)

(mostrar-elementos 1 2 3 4)

(mostrar-elementos 1 2 3 4 5)


;; Podemos observar como la primera y última expresión dan error alegando que hay un número incorrecto de argumentos.

;; Y si no sabemos cuantos argumentos vamos a tener? Para eso tenemos otra opción

;; >>> Evalúa las siguientes expresiones

(defun sumar (&rest numeros)
  (apply #'+ numeros))

(sumar 2)

(sumar 2 3)

(sumar 2 3 4)

(sumar 2 3 4 5)


;; -----------------------------------------------------------------------------------------------

;; >>>>> Un extra solo para crear funciones para Emacs, el programa que supongo que estás usando <<<<<<<<<<<<<<<<<<<

;; Además de crear funciones en Emacs podemos hacerlas interactivas, esto es poder llamarlas con el comando `Alt-x' y poder escribirla. Vamos a verlo con un ejemplo muy sencillo.

;; Hasta ahora si usamos la primitiva `print' nos muestra un mensaje, pero si usamos `message' lo hace en el mini-buffer.

;; >>> Evaluemos lo siguiente

(message "Hola a todos!")


(defun saludos ()
  "Esto es una función que nos saluda"
  (interactive)
  (message "Hola"))

;; Si pulsamos Alt-x y escribimos saludos en el minibuffer nos mostrará Hola


(defun saludos (nombre)
  (message "Hola %s" nombre))

(saludos 'paco)

;; Aquí vemos como nos saluda usando el nombre de paco. Pero podemos hacer que nos pregunte por el nombre que queremos mostrar en el mini-buffer?

;; >>> Evalúa lo siguiente

(defun saludos (nombre)
  "Esto es una función que nos saluda"
  (interactive "sEscribe tu nombre ")
  (message "Hola %s" nombre))
saludos



;; Ahora vamos a crear una combinación de teclas que llame a nuestra función.

(global-set-key (kbd "C-c s") 'saludos)


;; Ahora si pulsamos la combinación Control C y S aparecerá en nuestro buffer "Escribe tu nombre".




;;; Local variables:
;;; mode: lisp-interaction
;;; End:

;;; el tutorial sobre las funciones termina aquí
