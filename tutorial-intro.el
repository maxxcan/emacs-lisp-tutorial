;; tutorial-intro.el

;; Copyright (C) 2021, Maxxcan
;; This file is part of a Maxxcan Emacs Lisp tutorial, released under GNU GPL with ABSOLUTE NO WARRANTY. See the file COPYING for details.

;; Este tutorial está basado en el tutorial creado por Thien-thi Nguyen que podemos encontrar <<aquí>>

;; Audiencia
;; =========
;; Este tutorial presupone que se tenga alguna familiaridad con el manejo de Emacs como usuario. En otras palabras que si indicamos que se tiene que ejecutar las intrucciones "Escribe `C-x i'", se sepa lo que se tiene que hacer.

;; Si éste no fuera el caso podemos visitar otro tutorial para principiantes que hice en la siguiente dirección.


;; Método 
;; ======
;; Este tutorial es presentado en un fichero .el con comentarios y código en Emacs Lisp. Así que para llevarlo acabo hay que descargarse estos ficheros en un mismo directorio y además ejecutarlos en Emacs. Al final del archivo hay unas directrices para hacer este documento interactivo. Si esto no funcionase lo puedes hacer por tí mismo escribiendo

;; M-x lisp-interactive-mode RET

;; En este modo, `C-j' realiza la operación `eval-print-last-sexp', el cual modifica el buffer y e inserta el valor de la última expresión estructurada evaluada. 

;; Empecemos pues.

;; Evaluación
;; =========

;; La parte de este archivo que comienza sus líneas con ";" no tienen ningún "valor", pero si no lo tienen las otras líneas hacen cosas. Vamos a ver como interaccionar con este archivo.

;; >> Pon el cursor después del "3" en la próxima línea y teclea `C-j'.

3

;; Veremos como aparece otro 3. Esto es porque "3" tiene el valor de 3, en otras palabras, la representación de 3 evalúa a 3. Escribamos otro número y poniendo el cursor de ese número presionemos otra vez `C-j'

;; ¿Qué ocurre si lo hacemos otra vez pero poniendo el cursor delante del número y no detrás?

;; Ahora vamos a evaluar lo siguiente

"Emacs Lisp mola"

;; Como vemos esto es una cadena de caracteres siempre que esté rodeado de comillas y al igual que los números se autoevalúa a sí mismo. En general tales expresiones se llaman literales.

;; Vamos a ver un poco como funciona la ayuda que nos da Emacs. 

;; >> Teclea `M-x describe-key RET C-j'.

;; Esto nos mostrará una descripción del comando que `C-j' ejecuta, que se llama `eval-print-last-sexp'. Una vez nos aparezca la ventana de la ayuda podemos quitarla presionando la tecla `q' o usando `C-x 1' para eliminarla.

;; Una "sexp" es una expresión estructurada (structured expression), la cual puede ser tan simple como hemos visto o mucho más compleja.

;; >> Evaluemos esta expresión estructurada (sexp).

(+ 3 42)

;; Aparecerá el número 45. Ya que esta expresión es 3 + 42.
;; Si es así, ¿porque lo ponemos con el símbolo + delante?

;; Bien tanto en ELisp como en todos los lenguajes de la familia Lisp tienen lo que se llama la notación prefija también llamada notación polaca. La anterior se llama notación infija. La notación prefija pone los símbolos delante de los operandos o datos y no entre ellos. Es más fácil verlo con un ejemplo:

;; Notación infija: 3 + 4

;; Notación prefija: + 3 4

;; A esa fórmula entre paréntesis se le llama expresión extructurada, s-expresión o s-exp y representa una estructura de datos en árbol. Esto tiene muchas implicaciones a nivel del desarrollo del interprete y el compilador pero esa parte la dejaremos para otra ocasión. De momento quedémonos en que primero va un operador (un primitivo) y luego el resto de los elementos, los operandos (datos).

;; >> Evaluemos esto:				

+ 3 42


;; Nos aparecerá un 42. Esto es porque al no haber paréntesis hay tres expresiones simbólicas, el operador +, el número 3 y el número 42. Dedende de donde pongas el cursor te dará un resultado distinto. Hay que tener cuidado donde se pone el cursor porque al pulsar `C-j' se evalúa lo que hay justo antes del cursor.

;; >> Ahora vamos a evaluar las siguientes expresiones.

(- 3 42)
(* 3 42)
(/ 3 42)
(% 3 42)

;; Así vemos lo que hacen distintos operadores y como nos da un resultado entero ya que es lo que hemos usado. En cambio si usamos números decimales, lo que en informática se llama números con coma flotante:

;; >> Evaluemos ahora las siguientes expresiones.

(- 3.0 42.0)
(* 3.0 42.0)
(/ 3.0 42.0)
(% 3.0 42.0)

;; Aquí vemos como todos los ejemplos dan un resultado decimal o número en coma flotante excepto el último que no admite este tipo de datos por lo que da un error. En Lisp a diferencia de otros lenguajes no hace falta indicarle el tipo de dato que es sino que él ya lo resuelve cuando haga falta.

;; >> Ahora evaluemos la siguiente expresión:

(+ "Emacs Lisp mola" "mucho")

;; Esto dará un error porque la función `+' espera argumentos de tipo entero, no una cadena de caracteres que es lo que le hemos pasado. Como vemos una cadena de caracteres es algo entre comillas. Aunque lo veremmos más adelante prueba a cambiar la función `+' por la función `concat'.

;; >> Ahora evaluemos la expresión:

(+ 3 (* 21 2))

;; La forma que tiene Lisp de evaluar esto es primero las expresiones internas y luego sustituye esa expresión por el valor resultante y lo usa para la expresiones más externas de una forma secuencial. En este caso primero multiplicamos 21 * 2 cuyo resultado es 42 y luego sumamos 3 + 42 dando 45.

;; Podemos ver esto por nosotros mismos situando el cursor en sitios distintos cuando pulsemos `C-j'. Por ejemplo si ponemos el cursor al final del todo nos devolverá 45 pero si lo situamos justo después del paréntesis que sucede al 2, veremos que nos devuele 42. Haz la prueba.

;; >> Vamos a evaluar esta expresión:

(+ 1 1 1 (* (+ 1 1 1) (+ 1 1 1 (* 2 2)) (* 2)))

;; A Lisp y todos los lenguajes basados en él siempre se les ha criticado que uno puede acabar con un montón de paréntesis. Esto tiene su parte de realidad, ya que podemos tener muchas expresiones dentro de otras y a veces es difícil de ver exactamente cuantos paréntesis hay que poner. Por supuesto, hay soluciones sencillas para esto.

;; La primera es la identación. Lisp es muy flexible con ésto, ni la requiere ni la repudia, es a elección del programador el usarla. Veamos dos ejemplos:

;; Esta expresión:

(+ 3 (* 2 (/ 1 4))) 

;; También podemos escribirla así:

(+ 3
   (* 2
      (/ 1 4)
      )
   )

;; O también esta forma que es la más recomendable a nivel de estilo:

(+ 3
   (* 2
      (/ 1 4)))


;; Pero cualquiera de las tres vale.

;; La segunda opción es que la mayoría de editores y por supuesto Emacs, suelen llevar de serie características para facilitar la visualización de los paréntesis. Además también hay paquetes para Emacs que ayudan a esto como el paquete rainbow-delimeters. Los paquetes en Emacs los veremos en el anexo. Pero como digo de serie ya podemos activar una ayuda para esto.

;; >> Evalúa la siguiente expresión:

(setq blink-matching-parent t)

;; Ahora hemos activado una característica que nos ayudará a ver los paréntesis. Veamoslo en acción.

;; >> Añade el último paréntesis y observa el cursor:

(blah blah bla

;; Ahora vamos a verlo con un ejemplo más complejo y veamos así si podemos ver cuantos paréntesis necesitamos.

(blah (blah (blah blah) blah) (blah blah (blah blah

;; Crea tus propios ejemplos y juega con los paréntesis.

					       
(+ 1 1 1 (* (+ 1 1 1) (+ 1 1 1 (* 2 2)) (* 2)))


;; Ejercicios




;;; Local variables:
;;; mode: lisp-interaction
;;; End:

;;; la introducción termina aquí




